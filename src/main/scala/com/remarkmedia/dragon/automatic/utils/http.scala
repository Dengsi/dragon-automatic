package com.remarkmedia.dragon.automatic.utils

import java.io.DataOutputStream
import java.net.URL

import org.slf4j.LoggerFactory

import scala.io.Source

/**
  * Created by dengsi on 2017/3/29.
  */
object http {
  private val logger = LoggerFactory.getLogger(getClass)

  def post(addr: String, payload: String) = {
    logger.debug(s"HTTP POST=>url: ${addr}, payload: ${payload}")

    val url = new URL(addr)
    val connection = url.openConnection()
    connection.setDoInput(true)
    connection.setDoOutput(true)
    connection.setRequestProperty("Content-Type", "application/json")

    val out = new DataOutputStream(connection.getOutputStream)
    out.write(payload.getBytes("UTF-8"))
    out.flush()
    out.close()

    val in = connection.getInputStream()
    logger.debug(s"HTTP RESP=>${Source.fromInputStream(in).mkString("")}")
    in.close()
  }
}