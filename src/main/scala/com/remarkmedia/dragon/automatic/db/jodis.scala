package com.remarkmedia.dragon.automatic.db

import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.wandoulabs.jodis.RoundRobinJedisPool
import org.slf4j.LoggerFactory
import redis.clients.jedis.{Jedis, JedisPoolConfig}

import scala.util.{Failure, Success, Try}

/**
  * Created by dengsi on 2017/3/28.
  */
object jodis {
  private val logger = LoggerFactory.getLogger(this.getClass)

  val config = {
    val config = new JedisPoolConfig()
    config.setMaxTotal(1024)
    config.setMaxIdle(128)
    config.setMaxWaitMillis(-1)
    config.setBlockWhenExhausted(true)
    config
  }

  val pool = RoundRobinJedisPool
    .create()
    .poolConfig(config)
    .curatorClient(codisProxy, 30000)
    .timeoutMs(12000)
    .zkProxyDir(s"/zk/codis/${codisDb}/proxy")
    .build()

  def apply[T: Manifest](process: Jedis => T): Option[T] = {
    Try {
      pool.getResource
    } match {
      case Success(resource) =>
        val result = Try {
          Option(process(resource))
        } match {
          case Success(option) => option
          case Failure(e) => logger.error("failed in jodis process", e); None
        }

        resource.close()
        result
      case Failure(e) =>
        logger.error("failed to get jodis resource", e); None
    }
  }
}