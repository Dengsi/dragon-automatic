package com.remarkmedia.dragon.automatic.db

import java.nio.ByteBuffer

import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.remarkmedia.dragon.automatic.task.FollowTask.VideoInfo
import com.remarkmedia.dragon.repository.protocol.entity.{ImageEntity, VideoEntity}
import io.github.junheng.akka.hbase.OHM
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.{HBaseConfiguration, HConstants, TableName}

/**
  * Created by dengsi on 2017/3/29.
  */
object hbase {
  implicit val ordering = new Ordering[Option[String]] {
    override def compare(x: Option[String], y: Option[String]): Int = {
      if (x.isEmpty && y.isEmpty) {
        0
      } else if (x.isEmpty && y.nonEmpty) {
        1
      } else if (x.nonEmpty && y.isEmpty) {
        -1
      } else {
        x.get.compareTo(y.get)
      }
    }
  }

  private val connection = {
    val configuration = HBaseConfiguration.create()
    configuration.set("hbase.zookeeper.quorum", zkQuorum)
    configuration.set("hbase.zookeeper.property.clientPort", zkPort)
    configuration.set("hbase.client.scanner.timeout.period", "86400000")
    ConnectionFactory.createConnection(configuration)
  }

  def crud[T](tableName: String)(process: (Table) => T) = {
    val table = connection.getTable(TableName.valueOf(tableName))
    table.setWriteBufferSize(1024 * 1024 * 8)
    try process(table) finally {
      table.close()
    }
  }

  def scan[T]
  (tableName: String,
   start: Array[Byte] = HConstants.EMPTY_START_ROW,
   end: Array[Byte] = HConstants.EMPTY_END_ROW
  )(process: (ResultScanner) => T) = {
    val table: Table = connection.getTable(TableName.valueOf(tableName))
    try {
      val _scan = new Scan(start, end)
      _scan.setCaching(100)
      val scanner = table.getScanner(_scan)
      try process(scanner) finally {
        scanner.close()
      }
    } finally {
      table.close()
    }
  }

  def searchImageUrl(postRowkey: Array[Byte]): Option[String] = {
    hbase.scan("image", start = postRowkey)(_.next(5)).map(OHM.fromResult[ImageEntity]).sortBy(_.imageType).headOption match {
      case Some(image) if image.originalUrl.nonEmpty => Some(image.originalUrl)
      case _ => None
    }
  }

  def searchVideoUrl(postRowkey: Array[Byte]): Option[VideoInfo] = {
    hbase.crud("video") { table =>
      val result = table.get(new Get(postRowkey))
      if (result.isEmpty) {
        None
      } else {
        val videoEntity = OHM.fromResult[VideoEntity](result)

        val coverUrlOption = videoEntity match {
          case video: VideoEntity if video.coverUrl.nonEmpty => video.coverUrl
          case video: VideoEntity if video.originalCoverUrl.nonEmpty => video.originalCoverUrl
          case _ => None
        }
        val videoUrlOption = videoEntity match {
          case video: VideoEntity if video.ossUrl.nonEmpty => video.ossUrl
          case video: VideoEntity if video.videoUrl.nonEmpty => video.videoUrl
          case video: VideoEntity if video.originalVideoUrl.nonEmpty => Some(video.originalVideoUrl)
          case _ => None
        }

        (coverUrlOption, videoUrlOption) match {
          case (Some(coverUrl), Some(videoUrl)) => Some(VideoInfo(coverUrl, videoUrl))
          case _ => None
        }
      }
    }
  }

  def postImageRowKey(postRowKey: Array[Byte], sequence: Int) = {
    val bbf = ByteBuffer.allocate(33)
    bbf.put(postRowKey)
    bbf.put(sequence.toByte)
    bbf.array()
  }

  def postRowKey(id: String, targetId: String, target: String, timestamp: Long): Array[Byte] = {
    postRowKey(id, profileRowKey(targetId, target), timestamp)
  }

  def postRowKey(id: String, profileKey: Array[Byte], timestamp: Long) = {
    val bbf = ByteBuffer.allocate(32)
    bbf.put(profileKey)
    bbf.putLong(Long.MaxValue - timestamp)
    bbf.put(DigestUtils.md5(id).slice(0, 7))
    bbf.array()
  }

  def profileRowKey(targetId: String, target: String) = {
    DigestUtils.md5(targetId + ":" + target)
  }
}
