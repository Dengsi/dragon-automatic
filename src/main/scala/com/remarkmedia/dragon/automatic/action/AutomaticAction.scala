package com.remarkmedia.dragon.automatic.action

import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.remarkmedia.dragon.automatic.db.jodis
import com.remarkmedia.dragon.automatic.task.FollowTask

/**
  * Created by dengsi on 2017/3/28.
  */
object AutomaticAction extends Action {
  def follow(param: FollowParam): ActionResult = {
    jodis { redis =>
      redis.sadd(redisFollowing, param.trendingId.toString)
    } match {
      case Some(code) if (code == 1) =>
        FollowTask.start(param)
        Right(plainResult("Trending follow success."))
      case Some(code) if (code == 0) => Left("Trending is already followed.")
      case _ => Left("Redis operate error.")
    }
  }

  def unfollow(trendingId: String): ActionResult = {
    jodis { redis =>
      redis.srem(redisFollowing, trendingId)
      redis.del(redisSentPost + trendingId)
    }

    Right(plainResult("OK"))
  }
}

/**
  *
  * @param followEndTime  跟踪结束时间
  * @param keepAlive      查询scroll持续时间
  * @param scrollSize     专题详情批量发送条数
  * @param scrollInterval 查询间隔（分钟）
  * @param trendingId     专题id
  * @param startTime      开始时间
  * @param endTime        结束时间
  * @param targets        平台列表
  * @param shouldKeywords 可包含的关键字列表
  * @param mustKeywords   必须包含的关键字列表
  * @param profileIds     相关账号
  * @param likesCount     最小点赞数
  * @param commentsCount  最小评论数
  * @param imageLevel
  * @param videoLevel
  */
case class FollowParam
(
  followEndTime: Long,
  keepAlive: Int,
  scrollSize: Int,
  scrollInterval: Int,
  trendingId: Long,
  startTime: Option[Long],
  endTime: Option[Long],
  targets: List[String] = Nil,
  shouldKeywords: List[String] = Nil,
  mustKeywords: List[String] = Nil,
  profileIds: List[String] = Nil,
  likesCount: Int = 0,
  commentsCount: Int = 0,
  imageLevel: Int = 0,
  videoLevel: Int = 0
)