package com.remarkmedia.dragon.automatic.action

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.model.ContentType
import akka.http.scaladsl.model.ContentTypes._
import akka.routing.RoundRobinPool
import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.remarkmedia.dragon.automatic.task.PersistTask
import org.json4s.jackson.JsonMethods.{compact, render}
import org.json4s.{DefaultFormats, Extraction}

/**
  * Created by dengsi on 2017/3/6.
  */
trait Action {
  implicit val formats = DefaultFormats

  implicit val system = ActorSystem("task")

  val persistTask = system.actorOf(RoundRobinPool(persistRouterCount).props(Props(new PersistTask())), "persist")

  val encoding = "UTF-8"

  def objectToJson(entity: Any): String = compact(render(Extraction.decompose(entity)))

  def objectToResult(entity: Any): (Array[Byte], ContentType) = objectToJson(entity).getBytes(encoding) -> `application/json`

  def plainResult(message: String): (Array[Byte], ContentType) = message.getBytes(encoding) -> `text/plain(UTF-8)`

  type ActionResult = Either[String, (Array[Byte], ContentType)]
}