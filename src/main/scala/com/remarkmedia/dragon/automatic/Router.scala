package com.remarkmedia.dragon.automatic

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import com.remarkmedia.dragon.automatic.action.AutomaticAction._
import com.remarkmedia.dragon.automatic.action.FollowParam
import org.json4s.jackson.JsonMethods._

/**
  * Created by dengsi on 2017/3/28.
  */
object Router {
  val automaticRoute = {
    post {
      path("automatic" / "removeKey") {
        parameter("trendingId") { trendingId =>
          complete(buildResponse(unfollow(trendingId)))
        }
      }
    } ~ post {
      path("automatic" / "followTopic") {
        entity(as[Array[Byte]]) { bytes =>
          complete(buildResponse(follow(parse(new String(bytes, encoding)).extract[FollowParam])))
        }
      }
    }
  }

  def buildResponse(result: ActionResult): HttpResponse = result match {
    case Right((data, contentType)) => HttpResponse(
      status = StatusCodes.OK,
      entity = HttpEntity(contentType, data)
    )
    case Left(error) => HttpResponse(
      status = StatusCodes.BadRequest,
      entity = error
    )
  }
}