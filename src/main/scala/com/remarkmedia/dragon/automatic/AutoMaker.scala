package com.remarkmedia.dragon.automatic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.Supervision.Decider
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.remarkmedia.dragon.automatic.Router._
import com.remarkmedia.dragon.zk.ZkDefineFactory
import com.typesafe.config.ConfigFactory
import io.github.junheng.akka.hbase.OHM
import org.slf4j.LoggerFactory

/**
  * Created by dengsi on 2017/3/28.
  */
object AutoMaker extends App {
  ZkDefineFactory.load("automatic")

  private val logger = LoggerFactory.getLogger(getClass)

  private val config = ConfigFactory.load()

  implicit val system = ActorSystem("AutoMaker")

  val decider: Decider = {
    case e: Throwable => {
      logger.error(s"Catch Throwable => ${e.getMessage}", e)
      Supervision.stop
    }
  }

  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))

  implicit val executionContext = system.dispatcher

  OHM.loadSchemas("com.remarkmedia.dragon.repository.protocol.entity")(system.log)

  Http().bindAndHandle(automaticRoute, config.getString("http.host"), config.getInt("http.port"))
}