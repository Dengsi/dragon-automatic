package com.remarkmedia.dragon.automatic.task

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.remarkmedia.dragon.automatic.utils.http._

/**
  * Created by dengsi on 2017/3/29.
  */
class PersistTask extends Actor {
  override def receive: Receive = {
    case meta: PersistMeta =>
      post(persistUrl, meta.data)
      TimeUnit.MINUTES.sleep(persistInterval)
  }
}

case class PersistMeta(trendingId: String, data: String)