package com.remarkmedia.dragon.automatic.task

import akka.actor.{Actor, ActorSystem, Props}
import com.remarkmedia.dragon.automatic.GlobalConfig._
import com.remarkmedia.dragon.automatic.action.{Action, FollowParam}
import com.remarkmedia.dragon.automatic.db.elasticsearch._
import com.remarkmedia.dragon.automatic.db.hbase._
import com.remarkmedia.dragon.automatic.db.jodis
import com.remarkmedia.dragon.automatic.task.FollowTask.{BatchTrendingInfo, ProfileInfo, TrendingInfo}
import org.elasticsearch.search.SearchHit
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

/**
  * Created by dengsi on 2017/3/28.
  */
class FollowTask extends Actor with Action {
  private val logger = LoggerFactory.getLogger(getClass)

  override def receive: Receive = {
    case param: FollowParam if (System.currentTimeMillis() > param.followEndTime) =>
      logger.info(s"The follow task has been finished, trending id: ${param.trendingId}")
      jodis { redis =>
        redis.del(redisSentPost + param.trendingId.toString)
        redis.srem(redisFollowing, param.trendingId.toString)
      }
      context.stop(self)
    case param: FollowParam =>
      val redisKey = redisSentPost + param.trendingId
      searchPost(param).map(hit => trendingDetails(hit, param, redisKey)).filter(_.nonEmpty).map(_.get).grouped(param.scrollSize).foreach { trendingInfoList =>
        val batchTrendingInfo = BatchTrendingInfo(trendingInfoList)
        val json = objectToJson(batchTrendingInfo)
        persistTask ! PersistMeta(param.trendingId.toString, json)
        jodis { redis =>
          redis.sadd(redisKey, trendingInfoList.map(_.postId): _*)
        }
      }
  }

  private def trendingDetails(hit: SearchHit, param: FollowParam, redisKey: String): Option[TrendingInfo] = {
    Try {
      val sourceMap = hit.getSource
      val targetId = sourceMap.get("targetId").toString
      val nickname = sourceMap.get("targetName").toString
      val avatar = sourceMap.get("targetAvatar").toString
      val target = sourceMap.get("target").toString
      val postId = target match {
        case "instagram" => sourceMap.get("id").toString.split(":")(0).split("_")(0)
        case _ => sourceMap.get("id").toString.split(":")(0)
      }

      jodis { redis =>
        redis.sismember(redisKey, postId)
      } match {
        case Some(exist) if (exist) => logger.debug(s"The post ${postId}->${param.trendingId} has already sent."); None
        case _ =>
          val profileInfo = new ProfileInfo(nickname, avatar, targetId, platformIdMap.getOrElse(target, "0"))
          val postType = sourceMap.get("type").toString
          val timestamp = sourceMap.get("timestamp").toString.toLong
          val content = sourceMap.get("content").toString
          val likescount = sourceMap.get("likesCount").toString.toInt
          val commentscount = sourceMap.get("commentsCount").toString.toInt

          val rowkey = postRowKey(sourceMap.get("id").toString, targetId, target, timestamp)
          val trendingInfo = TrendingInfo(param.trendingId, postId, timestamp, profileInfo, postType, content, None, None, likescount, commentscount)
          postType match {
            case "image" if likescount >= param.imageLevel => searchImageUrl(rowkey) match {
              case Some(imageUrl) => Some(trendingInfo.copy(imageUrl = Some(imageUrl)))
              case None => None
            }
            case "video" if likescount >= param.videoLevel => searchVideoUrl(rowkey) match {
              case Some(videoInfo) => Some(trendingInfo.copy(video = Some(videoInfo)))
              case None => None
            }
            case _ => None
          }
      }
    } match {
      case Success(option) => option
      case Failure(e) => logger.error(e.getMessage, e); None
    }
  }
}

object FollowTask {
  def start(param: FollowParam)(implicit system: ActorSystem) = {
    import param._
    val actor = system.actorOf(Props(new FollowTask()), s"FollowTask-${trendingId}")
    system.scheduler.schedule(0 minutes, scrollInterval minutes, actor, param)(system.dispatcher)
  }

  case class BatchTrendingInfo(data: List[TrendingInfo])

  case class TrendingInfo
  (
    subCategoryId: Long,
    postId: String,
    timestamp: Long,
    profile: ProfileInfo,
    postType: String,
    content: String,
    imageUrl: Option[String],
    video: Option[VideoInfo],
    likeCount: Int,
    commentCount: Int
  )

  case class ProfileInfo(nickname: String, avatar: String, targetId: String, platform: String)

  case class VideoInfo(coverUrl: String, videoUrl: String)

}