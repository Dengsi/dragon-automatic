package com.remarkmedia.dragon.automatic

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

/**
  * Created by dengsi on 2017/3/28.
  */
object GlobalConfig {
  private val config = ConfigFactory.load()

  // -------------- APP config ---------------
  val appConfig = config.getConfig("app")
  val platformIdMap = appConfig.getObject("platform").map { case (platform, _) =>
    platform -> appConfig.getString(s"platform.${platform}")
  }
  val redisFollowing = appConfig.getString("redis-key.following")
  val redisSentPost = appConfig.getString("redis-key.sentPost")

  // -------------- Persist config ---------------
  val persistConfig = config.getConfig("persist")
  val persistUrl = persistConfig.getString("url")
  val persistInterval = persistConfig.getInt("interval")
  val persistRouterCount = persistConfig.getInt("router-count")

  // -------------- HTTP config ---------------
  val httpConfig = config.getConfig("http")
  val host = httpConfig.getString("host")
  val port = httpConfig.getInt("port")

  // -------------- HBASE config ---------------
  val hbaseConfig = config.getConfig("hbase")
  val zkQuorum = hbaseConfig.getString("zookeepers")
  val zkPort = hbaseConfig.getString("zookeepers-port")

  // -------------- ES config ---------------
  val esConfig = config.getConfig("elasticsearch")
  val esId = esConfig.getString("id")
  val esUri = esConfig.getString("uri")

  // -------------- Codis config ---------------
  val codisConfig = config.getConfig("codis")
  val codisProxy = codisConfig.getString("proxy")
  val codisDb = codisConfig.getString("db")
}